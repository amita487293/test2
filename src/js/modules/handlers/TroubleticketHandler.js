class TroubleticketHandler {

	TroubleticketCreate = (paramMap) => {
        console.log(paramMap);
		let finalMap;
		let date = new Date();
		let creationDate = date;
		let description = paramMap.issue_type;
		let customer_id = paramMap.customer_id;
		let href;
		finalMap = {
                'creationDate':creationDate,
                'description': description,
                'correlationId': paramMap.correlationId,
                'type' : paramMap.type,
                'severity' : paramMap.severity,
                'relatedParty' : [{
                	'href' : customer_id
                }]
        };
        var arr = [];
        for (let key in paramMap) {
            // check if the property/key is defined in the object itself, not in parent
            if(!(key == ("correlationId").toUpperCase() || key ==("issue_type").toUpperCase() 
            || key == ("description").toUpperCase() || key == ("customer_id").toUpperCase()
            || key == ("type").toUpperCase() || key == ("severity".toUpperCase()))){
                arr.push({"involvement": key,
                "reference":paramMap[key]}
              );
                console.log(arr);

            }           
        }

    let relatedObject = arr;
    finalMap.relatedObject = relatedObject;

        return finalMap;
	}
}
module.exports = TroubleticketHandler;