const rp = require('request-promise-native');
const PropertiesReader = require('properties-reader');
const prop = PropertiesReader('target/resources/application.properties');
const http = require("http");
const TroubleticketHandler = require('../handlers/TroubleticketHandler');
const troubleticketHandler = new TroubleticketHandler(); 

class Util {
    //get customer
    getCustomer = (customer_id) => {
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/${customer_id}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });
    }

    //get customer using id_google
    getCustomerGoogle = (id_google) => {
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/google/${id_google}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });
    }

    //get customer using id_whatsapp
    getCustomerWhatsapp = (id_whatsapp) => {
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/whatsapp/${id_whatsapp}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });
    }

    //get customer using id_fb
    getCustomerFB = (id_fb) => {
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/fb/${id_fb}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });
    }

    //get customer using id_twitter
    getCustomerTwitter = (id_twitter) => {
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/twitter/${id_twitter}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });
    }

    //link user's google profile
    linkCustomerGoogle(customer_id,id_google,mobile){
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/google/link/${customer_id}/${id_google}/${mobile}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });
    }

    //link user's whatsapp profile
    linkCustomerWhatsapp(customer_id,id_whatsapp,mobile){
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/whatsapp/link/${customer_id}/${id_whatsapp}/${mobile}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });
    }

    //link user's FB profile
    linkCustomerFB = (customer_id,id_fb,mobile) => {
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/fb/link/${customer_id}/${id_fb}/${mobile}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });  
    }

    //link user's Twitter profile
    linkCustomerTwitter = (customer_id,id_twitter,mobile) => {
        return rp(this.getProperty('microservices.cloud.customer-rest')+`/twitter/link/${customer_id}/${id_twitter}/${mobile}`)
        .then((customer)=>{
            return Promise.resolve(customer);
        })
        .catch((err)=>{
            console.error(err);
            return Promise.reject(err);
        });  
    }
    //get customer's billing profile
    getCustBillingAcc = (customer_id) => {
        return this.getCustomer(customer_id)
        .then((response) => {
            response = JSON.parse(response);
            let acc_no = response.customerAccountRef[0].id;
            let uri = this.getProperty('microservices.cloud.billingaccount-rest')+`?acc_no=${acc_no}`;
            return rp(uri)
            .then((response) => Promise.resolve(response))
            .catch((err) => {
                console.log(err);
                return Promise.reject(err);
            });
        })
        .then((response) => {
            return Promise.resolve(response);
        })
        .catch((err) => {return Promise.reject(err);});
    }   

    //returns security check event
    startSecurtiyCheck = (mainIntent) => {
        return {
            'name':'security_event',
            'parameters':{
                'prompt':'Please tell me your Account Number',
                'mainIntent': mainIntent,
                'count':'0'
            }
        };
    }

    //triggers next intent based on mainIntent
    //need to chanege
    triggerNextIntent = (mainIntent, customer_id) => {
        console.log('124');
        if (mainIntent === 'Change Payment Method - Debit - Acc') {
            console.log("line 183 ");
            return {
                'name': 'account_event',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Duplicate Bill Request') {
            return {
                'name': 'duplicate_bill_request',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Payment Method - yes') {
            return {
                'name': 'amount_event',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Payment Method - yes - yes - yes') {
            return {
                'name': 'validation_event',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Payment Method') {
            return {
                'name': 'Change Payment Method',
                'parameters': {
                    'customer_id': customer_id,
                    'credit_pay_time': 'monthly',
                    'payment_method': 'credit card'
                }
            };
        }
        else if (mainIntent === 'Change Payment') {
            return {
                'name': 'amount_event',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Bill Frequency Request') {
            return {
                'name': 'change_bill_frequency',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Validation Check') {
            return {
                'name': 'next_event',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Billing Date Request') {
            return {
                'name': 'change_billing_date',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Pay Bill') {
            return {
                'name': 'Pay_bill',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Bill Not Received Issue') {
            return {
                'name': 'bill_not_received_issue',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Bill Mode Ebill') {
            return {
                'name': 'change_bill_mode_ebill',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Bill Mode - PaperBill') {
            return {
                'name': 'change_bill_mode_-_paperbill',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Change Payement Method - Credit') {
            return {
                'name': 'change_payment_method_-_credit',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Customer Billing Address Update Request') {
            return {
                'name': 'customer_billing_address_update_request',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
        else if (mainIntent === 'Check Credit Limit') {
            return {
                'name': 'check_credit_limit',
                'parameters': {
                    'customer_id': customer_id
                }
            };
        }
    }

    //validation check method
    startValidation = (mainIntent, customer_id) => {
        console.log("261 inside start validation");
            return {
                'name':'validation_event',
            'parameters':{
                'customer_id': customer_id ,
                'mainIntent' : mainIntent,
                'count_acc' : 0
            }
        };
    }

    //starts otp check intent freshly
    startOtpCheck = (mainIntent, customer_id) => {
        return {
            'name':'otp_event',
        'parameters':{
            'customer_id': customer_id ,
            'count' : 0,
            'prompt':'Sure, An OTP has been sent to your registered mobile number. Please provide the OTP',
            'mainIntent' : mainIntent
            }
        };
    }

    //change bill frequency
    changeBillFreq = (_id, frequency, nextBillingDate) => {
        let uri = null;
        if(nextBillingDate === null || nextBillingDate === undefined)
            uri = this.getProperty("microservices.cloud.billingaccount-rest")+"/frequency?_id="+_id+"&frequency="+frequency+"&nextBillingDate=null";
		else
			uri = this.getProperty("microservices.cloud.billingaccount-rest")+"/frequency?_id="+_id+"&frequency="+frequency+"&nextBillingDate="+nextBillingDate;
		
		return rp(uri).then((response) => {return Promise.resolve(response);})
        .catch((err) => {
            console.log(err);
            return Promise.reject(err);
        });
    }

    //to generate random number
    generateRandomNumber = () =>{
        return Math.floor(Math.random() * 90000) + 10000;
    }

    //change creditLimit method
    changeCustCreditLimit = (_id, value) => {
            return rp(this.getProperty("microservices.cloud.billingaccount-rest")+"/creditLimit?_id="+_id+"&value="+value)
            .then((customer)=>{
                return Promise.resolve(customer);
            })
            .catch((err)=>{
                console.error(err);
                return Promise.reject(err);
            });  
        }

    //change billing date
    changeBillDate = (_id, billDate, nextBillDate) => {
        return rp(this.getProperty('microservices.cloud.billingaccount-rest')+"/billDate?_id="+_id+"&billDate="+billDate+"&nextBillingDate="+nextBillDate)
        .then((response) => {return Promise.resolve(response);})
        .catch((err) => {
            console.log(err);
            return Promise.reject(err);
        })
    }

    //change number to ordinal
    ordinal = (i) => {
        let suffixes = ["th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"];
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + suffixes[i % 10];
        }
    }

    //get proper date
	getProperDate = (date) => {
		if(date < 10)
			return '0'+date;
		else
			return date;
    }
    
    //get catalogue
    getCatalogue = () => {
        let catalogue = JSON.parse(this.getProperty('dialogflow.catalogue'));
        return catalogue;
    }

    //gets property from ../resources/application.properties
    getProperty = (pty) => {return prop.get(pty);}
    
    //gets locality details
    getLocality = (postcode,customer) => {
        let al = [];
        customer = JSON.parse(customer);
        let cust_postcode = customer.contactMedium[0].medium.postcode;
        if(cust_postcode === 'HA02FG' || cust_postcode === 'MA5 2FG')
        {
           if(postcode.toUpperCase() === 'TN9 2QX' || postcode.toUpperCase() === 'TN92QX')
            {
                al.push('1 Welton Close, Tonbridge, TN9 2QX');
                al.push('10 Welton Close, Tonbridge, TN9 2QX');
				al.push('11 Welton Close, Tonbridge, TN9 2QX');
                al.push('12 Welton Close, Tonbridge, TN9 2QX');
				al.push('13 Welton Close, Tonbridge, TN9 2QX');
            }
            else if(postcode.toUpperCase() === 'M44 6EE' || postcode.toUpperCase() === 'M446EE')
            {
                al.push('10 Chapel Road, Manchester, M44 6EE');
				al.push('11 Chapel Road, Manchester, M44 6EE');
				al.push('12 Chapel Road, Manchester, M44 6EE');
				al.push('13 Chapel Road, Manchester, M44 6EE');
            }
            else
            {
                al.push('wrong postcode');
            }
        }
        else if(cust_postcode === '411057')
        {
            if(postcode === '411013')
            {
                al.push('B-102, Trillium, Magarpatta, 411013');
                al.push('C-10, Cosmos, Magarpatta, 411013');
                al.push('C-10, Cosmos, Magarpatta, 411013');
                al.push('H-401, Jasminium, Magarpatta, 411013');
                al.push('A-202, Iris, Magarpatta, 411013');
            }
            else
            {
                al.push('wrong postcode');
            }
        }   
    return al;
    }

    //change bill mode of customer
    changeBillMode = (_id,payMode) => {
        return rp(this.getProperty('microservices.cloud.billingaccount-rest')+"/billMode?_id="+_id+"&payMode="+payMode)
        .then((response) => {
            return Promise.resolve(response);
        })
        .catch((err) => {
            console.log(err);
            return Promise.reject(err);
        })        
    }

    //starts to live agent intent
    startLiveAgent = () =>{
        return {
            'name':'live_agent',
        };
    }

    //triggers next intent based on mainIntent 3 parameters
   triggerNextIntent2 = (mainIntent,customer_id,email_id) => {
        let followupEvent;
        if(mainIntent === "Customer Billing Address Update Request"){
            let SRno = this.generateRandomNumber();
                  followupEvent = {
                      'name':'address_update',
                      'parameters':{
                          'service_id': "SR"+SRno
                      }
                  };
        }
        else if(mainIntent === "Change Bill Mode Ebill - yes - email"){
            console.log("OTP Sucess in trigger event"+customer_id +email_id);
            if(customer_id.includes(".")){
                let arr = customer_id.split(".");
                customer_id = arr[0];
            }
            return this.changeCustEmailId(customer_id,email_id)
                then(()=>{
                    followupEvent = {
                        'name' : "ebill_yes_event"
                    };
                });         
        }
        //change bill mode paper bill
        else if(mainIntent==="Change Bill Mode - PaperBill - yes - yes - no - yes"){
                  
            console.log("Change Bill Mode - PaperBill - yes - yes - no - yes");
            followupEvent = {
                'name' : 'billchange_event'
            }
        }	
        else if(mainIntent==="Change Bill Mode - PaperBill - yes - yes - yes"){
            
            console.log("billchange event");
            followupEvent = {
                'name' : 'billchange_event'
            }
        }
        else if(mainIntent==="Change Payment Method - Debit - Acc - Otp"){
            
            console.log("jointaccount event");
            followupEvent = {
                'name' : 'jointaccount_event'
            }		
        }
        return followupEvent;        
    } 

    //change pay bill method
    changePayBillMethod = (_id,paymentMethod) => {
        return rp(this.getProperty('microservices.cloud.billingaccount-rest')+"/paymentMean?_id="+_id+"&paymentMethod="+paymentMethod)
        .then((response) => {
            return Promise.resolve(response);
        })
        .catch((err) => {
            console.log(err);
            return Promise.reject(err);
        });
    }

    //change customer Email Id
    changeCustEmailId = (_id,emailAddress) => {
        return rp(this.getProperty("microservices.cloud.customer-rest")+"/emailid?_id="+_id+"&emailAddress="+emailAddress)
        .then((response) => {
            return Promise.resolve(response);
        })
        .catch((err) => {
            console.log(err);
            return Promise.reject(err);
        });
    }
    
    startLiveAgent1 = (prompt) => {
    let followupEvent = {
            'name' :'live_agent',
            'parameters':{
            'prompt': prompt
            }
        };
        return followupEvent;
    }

    createTroubleTicket = (paramMap) => {
        let finalMap = troubleticketHandler.TroubleticketCreate(paramMap);
        let options = {
          method: 'POST',
          uri: 'https://ticket-rest-cloud.herokuapp.com/troubletickets',
          body: finalMap,
          headers: {
              'Content-Type': 'application/json',
          },
          json: true
        };
         
        return rp(options)
        .then(function (parsedBody) {
            return Promise.resolve(parsedBody);
        })    
        .catch(function (err) {
            console.log(err);
            return Promise.reject(err);
        });    
    }
}

module.exports = Util;