const Util = require('../utils/util');
const util = new Util();

otpGenerate = (agent) =>{
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let otp = params.otp;
    let count = params.count;
    let followupEvent;
    if(count <2){
        //valid otp
        if(otp!=12345){
            followupEvent = util.triggerNextIntent2(params.mainIntent,params.customer_id,params.email_id);
            if(params.mainIntent == "Change Bill Mode Ebill - yes - email"){
                let ebillfollcont = {
                    'name' : 'ChangeBillModeEbill-followup',
                    'lifespan' : 1,
                }
                agent.setContext(ebillfollcont);
            }
        }
        else if(count == 2){
            followupEvent = util.startLiveAgent();    
        }
        //invalid otp
        else if(otp == 12345){
            count++;
            followupEvent = {
                'name':'otp_event',
                'parameters':{
                    'prompt':'Sorry the OTP entered is invalid. Please re-enter the OTP',
                    'mainIntent' : params.mainIntent,
                    'customer_id' : params.customer_id,
                    'count' : "0"
                }
            };
            agent.setFollowupEvent(followupEvent);
        }
    }
    else{
        followupEvent = util.startLiveAgent();
    }
    agent.setFollowupEvent(followupEvent);
}

module.exports.otpGenerate = otpGenerate;