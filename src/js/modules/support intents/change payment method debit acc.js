const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

changePaymentMethodDebitAcc = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let account_number = params.account_number;
    let banksort_code = params.banksort_code;
    let account_name = params.account_name;
    console.log("account number,code,name"+account_number+""+banksort_code+""+account_name);
    agent.add("Thank you, I have captured your details for direct debit as - account number - "+account_number+" bank sort code -  " +banksort_code+",  and account name - "+account_name+". Your first bill amount will be debited 10 working days from date of your next bill.  Are you happy to proceed ?");
    agent.add(new Suggestion('Yes'));
    agent.add(new Suggestion('No Thanks'));
}

changePaymentMethodDebitAccOtp = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    if(customer_id != null){
        followupEvent = util.startOtpCheck("Change Payment Method - Debit - Acc - Otp",customer_id)
        agent.setFollowupEvent(followupEvent);
    }
    else{
        agent.add("This request will require you to login to self-care. Please login and try again.");
    }
}

jointAcc = (agent) => {
    agent.add("Is this a joint account?");
}

jointAccNo = (agent) =>{
    let srNum = util.generateRandomNumber();
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let payment_type = params.payment_type;

    return util.getCustBillingAcc(customer_id)
    .then( (custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        console.log("payment type is " +payment_type+customer_id);
        return util.changePayBillMethod(custBillingAcc._id, payment_type)
        .then( () => {
            agent.add("Thank you for verification, your payment method has been changed successfully to "+"'" + payment_type + "'"+".  The transaction reference is SR"+srNum);
        })
    })
}

module.exports.changePaymentMethodDebitAcc = changePaymentMethodDebitAcc;
module.exports.changePaymentMethodDebitAccOtp = changePaymentMethodDebitAccOtp;
module.exports.jointAcc = jointAcc;
module.exports.jointAccNo = jointAccNo;