const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');
const catalogue = util.getCatalogue();

endingNote = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
     agent.add('Is there anything else I can help you with?');
     agent.add(new Suggestion('Yes'));
     agent.add(new Suggestion('No'));
}
endingNoteYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    agent.add('Please tell me how can i assist you?');
    for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
}

module.exports.endingNote = endingNote;
module.exports.endingNoteYes = endingNoteYes;