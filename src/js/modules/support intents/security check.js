const Util = require('../utils/util');
const util = new Util();

securityCheck = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let count = params.count;
    let customer_id = params.customer_id;
    console.log("In security check tele");

    if(count<=2){
        return util.getCustomer(customer_id).then((response) => {
            if(response){
                let security_event_start = {
                    'name':'security_event_start',
                    'parameters':{
                        'mainIntent':params.mainIntent,
                        'customer_id':customer_id
                    }
                };
                agent.setFollowupEvent(security_event_start);
            }
            else if(count==2)
               agent.setFollowupEvent('live_agent');
            else{
                count++;
                let security_event = {
                    'name':'security_event',
                    'parameters':{
                        'prompt':"I couldn't find your account number, please provide a valid account number. You have "+(3-count)+" attempts left.",
                        'mainIntent':params.mainIntent,
                        'count':count+''
                    }
                }
                agent.setFollowupEvent(security_event);
            }
        })
        .catch((err) => {
            agent.add('Sorry the security check feature is currently down. We are working to resolve it.');
            return Promise.resolve();
        });
    }
    //when count is exceeded
    else
        agent.setFollowupEvent('live_agent');
}

securityCheck1 = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let count = params.count;
     console.log("In security check securityCheck1");
    if(count<=2){
        if(params.question_1.toUpperCase() === 'BANANA'){
            let security_event_2 = {
                'name':'security_event_2',
                'parameters':{
                    'prompt':'Security question 2: What is your favorite sport?',
                    'mainIntent':params.mainIntent,
                    'customer_id':params.customer_id
                }
            };
            agent.setFollowupEvent(security_event_2);                                         
        }
        else if(count==2)
            agent.setFollowupEvent('live_agent');
        else{
            count++;
            let security_event_1 = {
                'name':'security_event_1',
                'parameters':{
                    'prompt':"Response entered was wrong, what is your favorite fruit? You have "+(3-count)+" attempts left.",
                    'mainIntent':params.mainIntent,
                    'customer_id':params.customer_id,
                    'count':count+'',
                    'question_1':null
                }
            }
            agent.setFollowupEvent(security_event_1);            
        }
    }
    //when count is exceeded
    else
        agent.setFollowupEvent('live_agent');
}

securityCheck2 = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let count = params.count;
    let mainIntent = params.mainIntent;
    let customer_id = params.customer_id;
    if(count<=2){
        if(params.question_2.toUpperCase() === 'FOOTBALL'){
            let context1 = {
                name : 'securitycheck-followup',
                lifespan : -1
            }
            agent.setContext(context1);
            let event = util.triggerNextIntent(mainIntent, customer_id);
            console.log(event);
            agent.setFollowupEvent(event);
        }
        else if(count==2)
            agent.setFollowupEvent('live_agent');
        else{
            count++;
            let security_event_2 = {
                'name':'security_event_2',
                'parameters':{
                    'prompt':"Response entered was wrong, What is your favorite sport? You have "+(3-count)+" attempts left.",
                    'question_2':null,
                    'mainIntent':params.mainIntent,
                    'customer_id':params.customer_id,
                    'count':count+''
                }
            };
            agent.setFollowupEvent(security_event_2);
        }
    }
    //when count is exceeded
    else
        agent.setFollowupEvent('live_agent');
}

module.exports.securityCheck = securityCheck;
module.exports.securityCheck1 = securityCheck1;
module.exports.securityCheck2 = securityCheck2;