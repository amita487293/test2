const Util = require('../utils/util');
const util = new Util();
const { Suggestion } = require('dialogflow-fulfillment');

validationCheck = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let count_acc = params.count_acc;
    let account_number = params.account_number;
    let banksort_code = params.banksort_code;
    let account_name = params.account_name;
    if(account_number == '71454657' && banksort_code == '600865' && account_name.toUpperCase() == 'JOHN SMITH') {
        let followupEvent = {
            'name': 'account_event',
            'parameters': {
                'customer_id': customer_id
            }
        };
        agent.setFollowupEvent(followupEvent);
    }
    else if(account_number == '71454657' && banksort_code == '600865' && account_name.toUpperCase() == 'PHARRELL WILLAMS') {
        let followupEvent = {
            'name': 'account_event',
            'parameters': {
                'customer_id': customer_id
            }
        };
        agent.setFollowupEvent(followupEvent);
    }
    else if(account_number == '71454657' && banksort_code == '600865' && account_name.toUpperCase() == 'MILAN DANI') {
        let followupEvent = {
            'name': 'account_event',
            'parameters': {
                'customer_id': customer_id
            }
        };
        agent.setFollowupEvent(followupEvent);
    }
    else if(account_number == '71454657' && banksort_code == '600865' && account_name.toUpperCase() == 'MANOHARAN RAMSUDHARSAN') {
        let followupEvent = {
            'name': 'account_event',
            'parameters': {
                'customer_id': customer_id
            }
        };
        agent.setFollowupEvent(followupEvent);
    }
    else {
        let next_event = {
            'name': 'next_event',
            'parameters': {
                'prompt': 'Your entered bank details are incorrect.Please re-enter your bank deatils'
            }
        };
        agent.setFollowupEvent(next_event);
    }
}

validationCheckYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let msg = params.msg;
    let count_acc = params.count_acc;

    if (count_acc <= 1) {
        if (msg != null) {
            count_acc++;
            let followupEvent = util.startValidation(params.mainIntent, params.customer_id)
            followupEvent.parameters.count_acc = count_acc;
            agent.setFollowupEvent(followupEvent);
        }
        else {
            agent.setFollowupEvent('live_agent');
        }
    }
    else {
        agent.setFollowupEvent('live_agent');
    }
}

module.exports.validationCheck = validationCheck;
module.exports.validationCheckYes = validationCheckYes;