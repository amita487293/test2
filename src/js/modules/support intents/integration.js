const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');
//suggestoins catalogue
const catalogue = util.getCatalogue();

//handles Google welcome
googleWelcome = (agent) => {
    let request = agent.request_.body;
    //conversation start using GA
    if(request.queryResult.queryText === 'GOOGLE_ASSISTANT_WELCOME'){
        let id_google = request.originalDetectIntentRequest.payload.user.userId;
        return util.getCustomerGoogle(id_google)
        .then((customer) => {
            if(!customer){
                agent.add('If you are a our customer, you can link your google account to help me serve better. Do you want to proceed?');
                agent.add(new Suggestion('Yes'));
                agent.add(new Suggestion('No'));
            } 
            else{
                let context = {
                    name : 'googleintegration-followup',
                    lifespan : -1
                }
                agent.setContext(context);
               // agent.clearContext('googleintegration-followup');
                customer = JSON.parse(customer);
                agent.add(`Hi ${customer.name}! How may I help you?`);
                for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
            }   
        })
        .catch((err) => {
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    //intentional linking by user
    else{
        //request coming from GA
        if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google'){
            let id_google = request.originalDetectIntentRequest.payload.user.userId;
            return util.getCustomerGoogle(id_google)
            .then((customer) => {
                if(!customer) 
                    agent.setFollowupEvent('link_google');
                else{
                    let context = {
                        name : 'googleintegration-followup',
                        lifespan : -1
                    }
                    agent.setContext(context);
                   // agent.clearContext('googleintegration-followup');
                    agent.add(`Your profile has already been linked with us. How may I help you further?`);
                    for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));                    
                }
            })
            .catch((err) => {
                agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience');
                return Promise.resolve();
            });
        }
        //invalid request which is not from GA
        else{
            let context = {
                name : 'googleintegration-followup',
                lifespan : -1
            }
            agent.setContext(context);
           // agent.clearContext('googleintegration-followup');
            agent.add('In order to link, you need to request directly from using Google Assistant.');
        }
    }
}

//handles google integration
googleIntegration = (agent) => {
    let request = agent.request_.body;
    let id_google = request.originalDetectIntentRequest.payload.user.userId;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let mobile = params.mobile;
    
    return util.linkCustomerGoogle(customer_id,id_google,mobile)
    .then((customer) => {
        if(!customer)
            agent.add("I couldn't find your account, kindly verify the details entered by you and try again later");
        else{
            customer = JSON.parse(customer);
            agent.add(`Your account has been successfully linked with us. Welcome ${customer.name}! How may I help you further?`);
            for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
        }
    })
    .catch((err) => {
        agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience')
        return Promise.resolve();
    }); 
}

//starts intentional whatsapp integration by the user
whatsappWelcome = (agent) => {
    let request = agent.request_.body;
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;
    
    //request coming from whatsapp
    if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp =  payload.id_whatsapp;
        return util.getCustomerTwitter(id_whatsapp)
        .then((customer) => {
            if(!customer)
                agent.add('In order to link you Whatsapp profile I need your account details. Do you wish to proceed?');
            else{
                let context = {
                    name : 'whatsappintegration-followup',
                    lifespan : -1
                }
                agent.setContext(context);
             //   agent.clearContext('whatsappintegration-followup');
                agent.add(`Your profile has already been linked with us. How may I help you further?`);
            }    
        })
        .catch((err) => {
            agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience');
            return Promise.resolve();
        });
    }
    //invalid request which is not from whatsapp
    else{
        let context = {
            name : 'whatsappintegration-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('whatsappintegration-followup');
        agent.add('In order to link, you need to request directly from Whatsapp.');
    }
}

//handles whatsapp integration
whatsappIntegration = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let mobile = params.mobile;
    let id_whatsapp = request.originalDetectIntentRequest.payload.data.id_whatsapp;
    
    return util.linkCustomerWhatsapp(customer_id,id_whatsapp,mobile)
    .then((customer) => {
        if(!customer)
            agent.add("I couldn't find your account, kindly verify the details entered by you and try again later");
        else{
            customer = JSON.parse(customer);
            agent.add(`Your account has been successfully linked with us. Welcome ${customer.name}! How may I help you further?`);
        }
    })
    .catch((err) => {
        agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience')
        return Promise.resolve();
    }); 
}

//handles facebook welcome
facebookWelcome = (agent) => {
    let request = agent.request_.body;
    //conversation start using Messenger/FB 'Get Started'
    if(request.queryResult.queryText === 'FACEBOOK_WELCOME'){
        let id_fb = request.originalDetectIntentRequest.payload.data.sender.id;
        return util.getCustomerFB(id_fb)
        .then((customer) => {
            if(!customer){
                agent.add('If you are a our customer, you can link your facebook account to help me serve better. Do you want to proceed?');
                agent.add(new Suggestion('Yes'));
                agent.add(new Suggestion('No'));
            } 
            else{
                let context = {
                    name : 'fbintegration-followup',
                    lifespan : -1
                }
                agent.setContext(context);
            //    agent.clearContext('fbintegration-followup');
                customer = JSON.parse(customer);
                agent.add(`Hi ${customer.name}! How may I help you?`);
                for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
            }
        })
        .catch((err) => {
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    //intentional linking by user
    else{
        //request coming from facebook
        if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook'){
            let id_fb = request.originalDetectIntentRequest.payload.data.sender.id;
            return util.getCustomerFB(id_fb)
            .then((customer) => {
                if(!customer)
                    agent.setFollowupEvent('link_fb');
                else{
                    let context = {
                        name : 'fbintegration-followup',
                        lifespan : -1
                    }
                    agent.setContext(context);
                    //agent.clearContext('fbintegration-followup');
                    agent.add(`Your profile has already been linked with us. How may I help you further?`);
                    for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
                }
            })
            .catch((err) => {
                agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience');
                return Promise.resolve();
            });
        }
        //invalid request which is not from facebook
        else{
            let context = {
                name : 'fbintegration-followup',
                lifespan : -1
            }
            agent.setContext(context);
           // agent.clearContext('fbintegration-followup');
            agent.add('In order to link, you need to request directly from our FB page or from FB messenger.');
        }
    }
}

//handles facebook integration
facebookIntegration = (agent) => {
    let request = agent.request_.body;
    let id_fb = request.originalDetectIntentRequest.payload.data.sender.id;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let mobile = params.mobile;
    
    return util.linkCustomerFB(customer_id,id_fb,mobile)
    .then((customer) => {
        if(!customer)
            agent.add("I couldn't find your account, kindly verify the details entered by you and try again later");
        else{
            customer = JSON.parse(customer);
            agent.add(`Your account has been successfully linked with us. Welcome ${customer.name}! How may I help you further?`);
            for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
        }
    })
    .catch((err) => {
        agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience');
        return Promise.resolve();
    }); 
}

//starts intentional twitter integration by the user
twitterWelcome = (agent) => {
    let request = agent.request_.body;
    //request coming from twitter
    if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter'){
        let id_twitter = request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str;
        return util.getCustomerTwitter(id_twitter)
        .then((customer) => {
            if(!customer)
                agent.add('In order to link you Twitter profile I need your account details. Do you wish to proceed?');
            else{
                let context = {
                    name : 'twitterintegration-followup',
                    lifespan : -1
                }
                agent.setContext(context);
             //   agent.clearContext('twitterintegration-followup');
                agent.add(`Your profile has already been linked with us. How may I help you further?`);
            }    
        })
        .catch((err) => {
            agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience');
            return Promise.resolve();
        });
    }
    //invalid request which is not from twitter
    else{
        let context = {
            name : 'twitterintegration-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('twitterintegration-followup');
        agent.add('In order to link, you need to request directly from Twitter.');
    }
}

//handles twitter integration
twitterIntegration = (agent) => {
    let request = agent.request_.body;
    let id_twitter = request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let mobile = params.mobile;
    
    return util.linkCustomerTwitter(customer_id,id_twitter,mobile)
    .then((customer) => {
        if(!customer)
            agent.add("I couldn't find your account, kindly verify the details entered by you and try again later");
        else{
            customer = JSON.parse(customer);
            agent.add(`Your account has been successfully linked with us. Welcome ${customer.name}! How may I help you further?`);
        }   
    })
    .catch((err) => {
        agent.add('Service to link profiles is unavailable at the moment. Sorry for the inconvenience');
        return Promise.resolve();
    });
}
module.exports.googleWelcome = googleWelcome;
module.exports.googleIntegration = googleIntegration;
module.exports.facebookWelcome = facebookWelcome;
module.exports.facebookIntegration = facebookIntegration;
module.exports.twitterWelcome = twitterWelcome;
module.exports.twitterIntegration = twitterIntegration;
module.exports.whatsappWelcome = whatsappWelcome;
module.exports.whatsappIntegration = whatsappIntegration;
