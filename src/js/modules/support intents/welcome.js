const Util = require('../utils/util');
const util = new Util();
const {Card,Suggestion, } = require('dialogflow-fulfillment');
//suggestoins catalogue
const catalogue = util.getCatalogue();

welcome = (agent) => {
    //console.log('Dialogflow Request body: ' + JSON.stringify(agent.request_.body));
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;
    
    //Svallo
    if(params.customer_id.length>0){
        
        let customer_id = params.customer_id;
        return util.getCustomer(customer_id)
        .then((customer) => {
            if(!customer) {
                agent.add('Customer ID is invalid. Please use a valid channel to use the services!');
            }
            else{
                customer = JSON.parse(customer);
                agent.add(`Hi ${customer.name}! How may I help you?`);
                
            }
            for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((customer) => {
            if(!customer) {
                agent.add('Hi! How may I help you?');
            }
            else{
                customer = JSON.parse(customer);
                agent.add(`Hi ${customer.name}! How may I help you?`);
            }
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        })
    }
    //Facebook
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook'){
        let id_fb = request.originalDetectIntentRequest.payload.data.sender.id;
        return util.getCustomerFB(id_fb)
        .then((customer) => {
            if(!customer) {
                agent.add('Hi! How may I help you?');
            }
            else{
                customer = JSON.parse(customer);
                agent.add(`Hi ${customer.name}! How may I help you?`);
            }
            for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    //Google
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google'){
        let id_google = request.originalDetectIntentRequest.payload.user.userId;
        return util.getCustomerGoogle(id_google)
        .then((customer) => {
            if(!customer) {
                agent.add('Hi! How may I help you?');
            }
            else{
                customer = JSON.parse(customer);
                agent.add(`Hi ${customer.name}! How may I help you?`);
            }
            for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    //Twitter
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter'){
        let id_twitter = request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str;
        return util.getCustomerTwitter(id_twitter)
        .then((customer) => {
            if(!customer) {
                agent.add('Hi! You can say "link my twitter profile" to link your twitter profile. How may I help you?');
            }
            else{
                customer = JSON.parse(customer);
                agent.add(`Hi ${customer.name}! How may I help you?`);
            }
            for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='GOOGLE_TELEPHONY'){
       // agent.add('Hi! How may I help you from Telephony?');
       agent.add(new TelephonySynthesizeSpeech({
           text : 'Hi! How may I help you from Telephony?'
         })
       );

       // for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
    }
    //Unlinked Customer
    else{
       
        agent.add('Hi! How may I help you?');
        for(i in catalogue)  agent.add(new Suggestion(catalogue[i]));
       
    }
}
module.exports=welcome;