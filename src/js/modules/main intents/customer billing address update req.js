const Util = require('../utils/util');
const util = new Util();
//const catalogue = util.getCatalogue();
const {Suggestion} = require('dialogflow-fulfillment');

custBillAddrUpReq = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'Customerbillingaddressupdaterequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        return util.getCustomer(customer_id)
        .then((response) =>{
            response = JSON.parse(response);
            let medium = response.contactMedium[0].medium;
            let street1 = medium.street1;
            let street2 = medium.street2;
            let city = medium.city;
            let stateOrProvince = medium. stateOrProvince;
            let country = medium.country;
            let postcode = medium.postcode;
            let address='';
            if(street1 != null && street1 !='')
             address = street1;
            if(street2 != null && street2 !='')
             address = address+' '+street2;
             address = address+' '+city+' '+stateOrProvince+' '+country+' '+postcode;
             agent.add('Your current address in the system is '+address+' Do you want to change it?')
             agent.add(new Suggestion('Yes'));
             agent.add(new Suggestion('No'));
        })
        .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
   }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'Customerbillingaddressupdaterequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('customerbillingaddressupdaterequest-followup');
        let security_event = util.startSecurtiyCheck('Customer Billing Address Update Request');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) => {
            if(response){
               return proceed(JSON.parse(response));
            }
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            console.log("err "+err);
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

CustBillAddUpReqYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let postcode = params.postcode;
    let out = 'Select your Locality/Road from the list below applicable for the postcode '+postcode;
    return util.getCustomer(customer_id)
    .then((response) => {
       let al = util.getLocality(postcode,response);
        if(al.includes("wrong postcode"))
			{
						let enter_postcode = {
                            'name':'enter_postcode',
                            'parameters':{
                                'prompt':'We are not able to find details for that post code, please try again'
                            }
                        };
                        agent.setFollowupEvent(enter_postcode);
            }
            else{
                agent.add(out+' ');
                for(i in al)  agent.add(new Suggestion(al[i]));
            }
    })
    .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
}

CustBillAddUpReqLocality = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    console.log('My pgm is correct');
    let followupEvent = util.startOtpCheck('Customer Billing Address Update Request', customer_id);
    console.log(followupEvent);
    console.log('My pgm is correct'+followupEvent);
    agent.setFollowupEvent(followupEvent);

}
CustBillAddUpReqLocalityFallback = (agent) => {
    let live_agent = {
        'name':'live_agent',
        'parameters':{
            'prompt':'I am sorry we are not able to locate your details. Let me transfer you to a live agent for assistance.'
        }
    };
    agent.setFollowupEvent(live_agent);
}

module.exports.custBillAddrUpReq = custBillAddrUpReq;
module.exports.CustBillAddUpReqYes = CustBillAddUpReqYes;
module.exports.CustBillAddUpReqLocality = CustBillAddUpReqLocality;
module.exports.CustBillAddUpReqLocalityFallback = CustBillAddUpReqLocalityFallback;