const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');
const {Card} = require('dialogflow-fulfillment');

payBill = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    securityCheckSkip = () => {
       
        agent.add('This request requires you to login into selfcare.');
    }

    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        //first response
        return util.getCustBillingAcc(customer_id)
        .then((response) => {
            response = JSON.parse(response);
            let dueAmount = response.bills[0].amountDue.value;
            let currency = response.bills[0].amountDue.unit;
           
        agent.add('You due amount is '+dueAmount+' '+currency+'. You can pay your bill by clicking on the link below:');
        //agent.add('This message is from Dialogflow\'s Cloud Functions for Firebase editor!');
        agent.add(new Card({
        title: 'Title: Billing Page',
        buttonText: 'click to pay your bill',
        buttonUrl: 'https://hjsvalloprod.techmahindra.com/selfcare/myBillsAndPayments'
      }));
    })
        .catch((err) => {
            console.log(err);
            return Promise.resolve();
        })
    }

    securityCheck = () => {
        let security_event = util.startSecurtiyCheck("Pay Bill");
        agent.setFollowupEvent(security_event);
    }

    //Svallo
    if(customer_id.length>0) 
    return util.getCustomer(customer_id)
    .then((response) =>{
        if(response)
            return proceed(JSON.parse(response));
        else
            agent.add('Invalid customer ID found. Please try again from any valid channel');
    })
    .catch((err) => {
        agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }        
    //Google
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
        if(response)
            return proceed(JSON.parse(response));
        else
            securityCheck();
    })
    .catch((err) => {
        agent.add('Our google channel is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
    //Facebook
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
    .then((response) => {
        if(response)
            return proceed(JSON.parse(response));
        else
            securityCheck();
    })        
    .catch((err) => {
        agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
    //Twitter
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
    .then((response) => {
        if(response)
            return proceed(JSON.parse(response));
        else
            securityCheck();
    })        
    .catch((err) => {
        agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
    else 
    securityCheckSkip();
}

module.exports.payBill = payBill;