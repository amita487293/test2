const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

changeBillModeEbill = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'ChangeBillModeEbill-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('ChangeBillModeEbill-followup');
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);

    //new code start
    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {

        custBillingAcc = JSON.parse(custBillingAcc);
        let billmode = custBillingAcc.customerBillPresentationMedia.name;
        //console.log("bill mode"+billmode);

        if(billmode.toLowerCase().includes("paper")){
            console.log("if block of paper "+billmode);
            agent.add("You are currently receiving paper bills at your registered address. Should I proceed to change it to Ebill?");
            agent.add(new Suggestion('yes'));
            agent.add(new Suggestion('no'));
        }
        else if(billmode.toLowerCase().includes("ebill")){
            console.log("if block of ebill "+billmode);
            agent.add('Your bill mode is already Ebill.');            
        }
        else{
            console.log("if block of common "+billmode);
            //set output context to go yes
            let changebillyescontext = {
                'name' : 'ChangeBillModeEbill-yes-followup',
                'lifespan' : 3,
            }
            agent.setContext(changebillyescontext);
            agent.add("You have subscribed to both email and paper bills.Do you want to discontinue paperbill? 11133");
            agent.add(new Suggestion('yes'));
            agent.add(new Suggestion('no'));
        }
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it . 111 2222');
        return Promise.resolve();
    });
    //end code
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
       // agent.clearContext('ChangeBillModeEbill-followup');
        let context = {
            name : 'ChangeBillModeEbill-followup',
            lifespan : -1
        }
        agent.setContext(context);
        let security_event = util.startSecurtiyCheck('Change Bill Mode Ebill');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
                return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response){
                return proceed(JSON.parse(response));
            }
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();

}


changeBillModeEbillYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    return util.getCustomer(customer_id)
    .then((response) => {
        return util.getCustBillingAcc(customer_id)
        .then((custBillingAcc) => {
            custBillingAcc = JSON.parse(custBillingAcc);
            let billmode = custBillingAcc.customerBillPresentationMedia.name;
            response = JSON.parse(response);
            let arr1 = response.contactMedium;
            let email = arr1[0].medium.emailAddress;
            console.log("email1 "+email);
            let email1 = email.replace("\"", "");
            if((null != email1) && (email1.length>0)){
                agent.add(`You will receive the ebill from next month at ${email1}. Should I discontinue paper bill from next month?`)
                agent.add(new Suggestion('yes'));
                agent.add(new Suggestion('no'));
            }
            else{
                agent.add("We don't have your email id registered. Can you please provide your email id ?")
            }
        })
    })
    .catch((err) => {
    	console.log("155 catch");
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

changeBillModeEbillYesNo = (agent) => {
    console.log("inside change bill mode ebill yes no");
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    return util.getCustomer(customer_id)
    .then((response) =>{
        return util.getCustBillingAcc(customer_id)
        .then((custBillingAcc) => {
           custBillingAcc = JSON.parse(custBillingAcc);
            let billmode = custBillingAcc.customerBillPresentationMedia.name;
            response = JSON.parse(response);
            let arr1 = response.contactMedium;
            let email = arr1[0].medium.emailAddress;
            let email1 = email.replace("\"", "");
            return util.changeBillMode(custBillingAcc._id,"Both")
            .then(() =>{
                let updatedContext = {
                    'name' : 'ChangeBillModeEbill-yes-followup',
                    'lifespan' : -1
                }
                agent.setContext(updatedContext);
                agent.add("You will receive the ebill from next month at "+email1+" and your paper bill will still be continued.. Your SR number is SR1234")
            })
        })
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

changeBillModeEbillYesYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    return util.getCustomer(customer_id)
    .then((response) =>{
        return util.getCustBillingAcc(customer_id)
        .then((custBillingAcc) => {
            custBillingAcc = JSON.parse(custBillingAcc);
            let billmode = custBillingAcc.customerBillPresentationMedia.name;
            console.log("bill mode "+billmode);
            response = JSON.parse(response);
            let arr1 = response.contactMedium;
            let email = arr1[0].medium.emailAddress;
            let email1 = email.replace("\"", "");
            return util.changeBillMode(custBillingAcc._id,"Ebill")
            .then(() =>{
               let updatedContext = {
                    'name' : 'ChangeBillModeEbill-yes-followup',
                    'lifespan' : -1
                }
                agent.setContext(updatedContext);
               // agent.clearContext('ChangeBillModeEbill-yes-followup');
                agent.add("You will receive the ebill from next month at "+email1+" and your paper bill will be discontinued.. Your SR number is SR1234")
            })
        })
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}


changeBillModeEbillYesEmail = (agent) =>{
    console.log("inside changeBillModeEbillYesEmail ");
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    console.log("customer id "+customer_id);
    let emailid = params.email_id;
    return util.getCustomer(customer_id)
    .then(() =>{
        if(null != emailid){
            let otp_event = {
                'name':'otp_event',
                'parameters':{
                    'prompt':'Thank you, I have sent an email to '+emailid+'. Can you please provide the validation code in the email  ?',
                    'mainIntent' : 'Change Bill Mode Ebill - yes - email',
                    'customer_id' : customer_id,
                    'email_id' : emailid,
                    'count' : "0"
                }
            };
            agent.setFollowupEvent(otp_event);              
        }    
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

module.exports.changeBillModeEbill = changeBillModeEbill;
module.exports.changeBillModeEbillYes = changeBillModeEbillYes;
module.exports.changeBillModeEbillYesNo = changeBillModeEbillYesNo;
module.exports.changeBillModeEbillYesYes = changeBillModeEbillYesYes;
module.exports.changeBillModeEbillYesEmail = changeBillModeEbillYesEmail;
