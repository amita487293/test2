const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

changePaymentMethod = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'ChangePaymentMethod-followup-2',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('ChangePaymentMethod-followup-2');
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        let payment_type  = params.payment_type;
        console.log("payment_type " +payment_type);
        return util.getCustBillingAcc(customer._id)
        .then((responseBill) => {
            responseBill = JSON.parse(responseBill);
            let paymentType = responseBill.paymentMean.name;
            console.log("paymentType "+paymentType+" and payment_type "+payment_type );
            console.log("payment_type length " +payment_type.length);
            if(paymentType.toLowerCase().includes("credit")){
                console.log("line 32");
                if((payment_type!=null) && (payment_type.length == 12 ||  payment_type.length == 11 || payment_type.localeCompare("direct debit") ||  payment_type.localeCompare("directdebit") || payment_type.localeCompare("debit card") ||  payment_type.localeCompare("debitcard"))){
                         agent.add("To change the payment method to Direct debit, do you want opt for Monthly or Quarterly payment");
                         agent.add(new Suggestion('Monthly'));
                         agent.add(new Suggestion('Quarterly'));
            }
            else if((payment_type!=null) && (payment_type.length == 7 || payment_type.localeCompare("payment"))){
                agent.add("Your current bill payment method is "+"'" + paymentType + "'"+" What do you want to change it to ? 1. Direct Debit Monthly 2. Direct Debit Quarterly 3. Credit Card Monthly 4. Credit Card Quarterly");
                agent.add(new Suggestion('Direct Debit Monthly'));
                agent.add(new Suggestion('Direct Debit Quarterly'));
                agent.add(new Suggestion('Credit Card Monthly'));
                agent.add(new Suggestion('Credit Card Quarterly'));
            }
            else if((payment_type!=null) && (payment_type.localeCompare("Direct Debit Monthly") ||  payment_type.localeCompare("Direct Debit Quarterly"))){
                console.log("inside change payment method " +payment_type);
                let updatedContext = {
                    'name' : 'ChangePaymentMethod-yes-followup',
                    'lifespan' : 1
                }
                agent.setContext(updatedContext);
                return util.triggerNextIntent("Change Payment",customer_id)
                .then((followupEvent) => {
                    agent.setFollowupEvent(followupEvent);
                })
            }
        }
        else{
            let updatedContext = {
                'name' : 'ChangePaymentMethod-followup-2',
                'lifespan' : -1,
            }
            agent.setContext(updatedContext);
            agent.add("Your current bill payment method is already "+paymentType+" .");
        }   
        })
        .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });        
    }

    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'ChangePaymentMethod-followup-2',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('ChangePaymentMethod-followup-2');
        let security_event = util.startSecurtiyCheck('Change Payment Method');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
               return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

changePaymentMethodYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let payment_type = params.payment_type;
    console.log("payment type is** " +payment_type);
    if((payment_type!=null) && (payment_type === ("Credit Card Monthly") ||  payment_type == ("Credit Card Quarterly"))){
        console.log("inside change payment method-yes if " +payment_type);
        let updatedContext = {
            'name' : 'changepayementmethod-credit-followup',
            'lifespan' : 1
        }
        agent.setContext(updatedContext);
        let followupEvent = {
            'name' : 'change_payment_method',
            'parameters':{
                'credit_pay_time' : 'monthly',
                'customer_id' : customer_id,
                'payment_method' : 'Credit Card'
            }
        };
        agent.setFollowupEvent(followupEvent);
    }
    else{
        console.log("inside change payment method-yes else" + payment_type);
        let followupEvent = {
            'name' : 'amount_event'
        };
        agent.setFollowupEvent(followupEvent);
    }

}

changePaymentMethodYesYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let payment_type = params.payment_type;
    console.log("payment type is: " +payment_type);
    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        let arr = custBillingAcc.billingAccountBalance;
        let amount = arr[0].amount;
        console.log("amount" + amount);

        if(null != amount && amount > 0){
            agent.add("You have an outstanding balance of " +amount+ " EUR. This amount will be included in next month's bill using direct debit");
        }
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

changePaymentMethodYesYesYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let payment_type = params.payment_type;
    console.log("Change Payment Method - yes - yes - yes");
    if((customer_id!=null) && (payment_type === ("Direct Debit Monthly") || payment_type === ("Direct Debit Quarterly"))){
        let followupEvent= util.startValidation("Change Payment Method - yes - yes - yes",customer_id)
            agent.setFollowupEvent(followupEvent);
    }
}

module.exports.changePaymentMethod = changePaymentMethod;
module.exports.changePaymentMethodYes = changePaymentMethodYes;
module.exports.changePaymentMethodYesYes = changePaymentMethodYesYes;
module.exports.changePaymentMethodYesYesYes = changePaymentMethodYesYesYes;