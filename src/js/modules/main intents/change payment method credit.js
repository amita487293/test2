const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

changePaymentMethodCredit = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'ChangePayementMethod-Credit-followup',
            lifespan : -1
        }
        agent.setContext(context);
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        let payment_method =params.payment_method;
        let credit_pay_time = params.credit_pay_time;
        console.log("payment method and pay time "+payment_method+credit_pay_time);
        return util.getCustBillingAcc(customer._id)
        .then((custBillingAcc) => {
            custBillingAcc = JSON.parse(custBillingAcc);
            let arr = custBillingAcc.billingAccountBalance;
            let paymentType = custBillingAcc.paymentMean.name;
            let status = arr[0].status;
            let amount = arr[0].amount;
            console.log("payment type status amount "+paymentType+status+amount);
            if(paymentType.toLowerCase().includes('debit')){
                 if(payment_method.includes('Credit') && (null != credit_pay_time) && (credit_pay_time.length > 0)){
                    //set output context to go in validation code
                    let updatedcontext = {
                        'name' : 'ChangePayementMethod-Credit-Paytype-followup',
                        'lifespan' : 3
                    }
                    agent.setContext(updatedcontext);
                    if((null != amount) && (amount.length>0)){
                        agent.add("Your current bill payment method is "+paymentType+". You have an outstanding balance of " +amount+ " EUR. This amount will be included in next month bill using credit card. I would need your credit card details. Please note that this is captured in a secured manner.");
                        agent.add(new Suggestion('Ok'));
                        agent.add(new Suggestion('No Thanks'));
                    }
                    else{
                        agent.add("Your current bill payment method is "+paymentType+". I would need your credit card details. Please note that this is captured in a secured manner.");
                        agent.add(new Suggestion('Ok'));
                        agent.add(new Suggestion('No Thanks'));
                    }
                 }
                 else{
                     agent.add("your current bill payment method is "+paymentType+".To change the payment method to Credit Card , do you want opt for Monthly or Quarterly payment?");
                     agent.add(new Suggestion('Monthly'));
                     agent.add(new Suggestion('Quarterly'));
                 }
            }
            else{
                // updating the ChangePayementMethod-Credit-followup -> updatedcontext
                let updatedcontext = {
                    'name' : 'ChangePayementMethod-Credit-followup',
                    'lifespan' : -1
                }
                agent.setContext(updatedcontext);
                agent.add(`Your current bill payment method is already ${paymentType}.`);
            }
        })
        .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'ChangePayementMethod-Credit-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('ChangePayementMethod-Credit-followup');
        let security_event = util.startSecurtiyCheck('Change Payement Method - Credit');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
               return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

changePaymentMethodCreditPaytype = (agent) => {
    console.log("inside changePaymentMethodCreditPaytype");
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let payment_method = params.payment_method;
    let credit_pay_time = params.credit_pay_time;
    console.log("payment_method credit pay time "+payment_method+credit_pay_time);
    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        let arr = custBillingAcc.billingAccountBalance;
        let paymentType = custBillingAcc.paymentMean.name;
        let status = arr[0].status;
        let amount = arr[0].amount;
        console.log("status amount "+status+amount);
        if((null != amount) && (amount.length>0)){
            agent.add("You have an outstanding balance of " +amount+ " EUR. This amount will be included in next month bill using credit card. I would need your credit card details. Please note that this is captured in a secured manner.");
            agent.add(new Suggestion('Ok'));
            agent.add(new Suggestion('No Thanks'));
        }
        else{
            agent.add("I would need your credit card details. Please note that this is captured in a secured manner.");
            agent.add(new Suggestion('Ok'));
            agent.add(new Suggestion('No Thanks'));
        }
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });

}

changePaymentMethodCreditPaytypeNo = (agent) => {
    console.log("inside changePaymentMethodCreditPaytypeNo ");
    let updatedcontext = {
        'name' : 'ChangePayementMethod-Credit-Paytype-followup',
        'lifespan' : -1
    }
    agent.setContext(updatedcontext);
    agent.setFollowupEvent('end_event');
}

changePaymentMethodCreditPaytypeValidation = (agent) => {
    console.log("changePaymentMethodCreditPaytypeValidation");
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let credit_card_no = params.credit_card_no;
    let cardholder_name = params.cardholder_name;
    let cvv_code = params.cvv_code;
    let expiry_date = params.expiry_date;
    let count_value = params.count_value;
    console.log("Change Payement Method - Credit - Paytype -validation count_value"+count_value);
    console.log("Change Payement Method - Credit - Paytype -validation All values "+credit_card_no +cardholder_name+cvv_code+expiry_date);
    let payment_method = params.payment_method;
    let credit_pay_time = params.credit_pay_time;
    console.log("Values in validation "+credit_pay_time + payment_method + customer_id);
    if(credit_card_no == "12345678" && cvv_code == "123" && cardholder_name.localeCompare("John Smith") && (expiry_date == "12/2020" ||expiry_date == "2020-12" || expiry_date == "20/12" || expiry_date == "12/20")){
        console.log(" validation success");
        console.log(" validation Sucess block set Lifespan 0");
        let updatedcontext = {
            'name' : 'ChangePayementMethod-Credit-Paytype-followup',
            'lifespan' : -1
        }
        agent.setContext(updatedcontext);
        console.log("printing substing");
        agent.add("Thank you, I have captured your details in the system for credit card number ending with "+credit_card_no+". Are you happy to proceed ?");
        agent.add(new Suggestion("Yes"));
        agent.add(new Suggestion("No"));
    }
    else if(credit_card_no == "12345678" && cvv_code == "123" && cardholder_name.localeCompare("Pharrell Willams") && (expiry_date == "12/2020" ||expiry_date == "2020-12" || expiry_date == "20/12" || expiry_date == "12/20")){
        console.log(" validation success");
        console.log(" validation Sucess block set Lifespan 0");
        let updatedcontext = {
            'name' : 'ChangePayementMethod-Credit-Paytype-followup',
            'lifespan' : -1
        }
        agent.setContext(updatedcontext);
        agent.add("Thank you, I have captured your details in the system for credit card number ending with "+credit_card_no+". Are you happy to proceed ?");
        agent.add(new Suggestion("Yes"));
        agent.add(new Suggestion("No"));
    }
    else if(credit_card_no == "12345678" && cvv_code == "123" && cardholder_name.localeCompare("Milan Dani") && (expiry_date == "12/2020" ||expiry_date == "2020-12" || expiry_date == "20/12" || expiry_date == "12/20")){
        console.log(" validation success");
        console.log(" validation Sucess block set Lifespan 0 line num232");
        let updatedcontext = {
            'name' : 'ChangePayementMethod-Credit-Paytype-followup',
            'lifespan' : -1
        }
        agent.setContext(updatedcontext);
        agent.add("Thank you, I have captured your details in the system for credit card number ending with "+credit_card_no+". Are you happy to proceed ?");
        agent.add(new Suggestion("Yes"));
        agent.add(new Suggestion("No"));
    }
    else{
        if(count_value < 1){
            console.log("validation failed");
            // setting follow up event to trigger the credit_val_next_event along with prompt
            let creditvalevent = {
                'name' : 'credit_val_next_event',
                'parameters':{
                    'prompt':'Your entered bank details are incorrect.Please re-enter your bank deatils. Shall we try again?',
                    'customer_id': params.customer_id
                }
            }
            agent.setFollowupEvent(creditvalevent);
        }
        else{
            console.log("validation fail else block set Lifespan 0");
            agent.setFollowupEvent('live_agent');
        }
    }
}

changePaymentMethodCreditPaytypeValidationFail = (agent) => {
    console.log("changePaymentMethodCreditPaytypeValidationFail");
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let msg = params.msg;

    if(msg != null && (msg.localeCompare('ok') || msg.localeCompare('ok fine') || msg.localeCompare('yes') || msg.localeCompare('sure'))){
        let creditvalevent = {
            'name' : 'credit_validation_event',
            'parameters' : {
                'customer_id' : customer_id,
                'count_value' : 1
            }
        }
        agent.setFollowupEvent(creditvalevent);
    }
    else if(msg != null && msg.localeCompare('no')){
        agent.setFollowupEvent('live_agent');
    }
    else{
        let updatedcontext = {
            name : 'ChangePayementMethod-Credit-Paytype-followup',
            lifespan : -1,
        }
        agent.setContext(updatedcontext);
        agent.setFollowupEvent('live_agent');
    }
}

changePaymentMethodCreditPaytypeValidationPass = (agent) => {
    console.log("insidechangePaymentMethodCreditPaytypeValidationPass");
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let payment_method = params.payment_method;
    let credit_pay_time = params.credit_pay_time;
    console.log("Values in validation pass "+credit_pay_time + payment_method + customer_id);
    console.log(" validation Pass intent block set Lifespan 0" );
    let updatedcontext = {
        'name': 'ChangePayementMethod-Credit-Paytype-validation-followup',
        'lifespan' : -1
    }
    agent.setContext(updatedcontext);
    agent.add("An OTP has been sent for verification to your registered mobile number. Please provide the OTP.");
}

changePaymentMethodCreditPaytypeValidationPassOTP = (agent) =>{
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    //Should i add promise function
    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        let payment_method = params.payment_method;
        let credit_pay_time = params.credit_pay_time;
        let otp_number = params.otp_number;
        console.log("Values in OTP "+credit_pay_time + payment_method);
        let updatedcontext = {
            'name' : 'ChangePayementMethod-Credit-followup',
            'lifespan' : -1
        }
        if(otp_number == 345678){
            //should i use call back;
            return util.changePayBillMethod(custBillingAcc._id,"credit card")
            .then(() => {
            agent.add("Thank you for verification, your payment method has been changed successfully to Credit card.The transaction reference is SR12345");
            })
        }
        else{
            agent.setFollowupEvent('live_agent');
        }
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

module.exports.changePaymentMethodCredit = changePaymentMethodCredit;
module.exports.changePaymentMethodCreditPaytype = changePaymentMethodCreditPaytype;
module.exports.changePaymentMethodCreditPaytypeNo = changePaymentMethodCreditPaytypeNo;
module.exports.changePaymentMethodCreditPaytypeValidation = changePaymentMethodCreditPaytypeValidation;
module.exports.changePaymentMethodCreditPaytypeValidationFail = changePaymentMethodCreditPaytypeValidationFail;
module.exports.changePaymentMethodCreditPaytypeValidationPass = changePaymentMethodCreditPaytypeValidationPass;
module.exports.changePaymentMethodCreditPaytypeValidationPassOTP = changePaymentMethodCreditPaytypeValidationPassOTP;