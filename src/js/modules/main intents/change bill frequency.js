const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

changeBillFrequency = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'changebillfrequencyrequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
        //agent.clearContext('changebillfrequencyrequest-followup');
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        return util.getCustBillingAcc(customer._id)
        .then((response) => {
            let billingCycle = JSON.parse(response).customerBillingCycleSpecification;
            let frequency = billingCycle.frequency;

            if(params.hasOwnProperty('choice') && frequency===params.choice){
                let context = {
                    name : 'changebillfrequencyrequest-followup',
                    lifespan : -1
                }
                agent.setContext(context);
              //  agent.clearContext('changebillfrequencyrequest-followup');
                agent.add("Your bill is already generated on "+frequency+" basis.");
            }
            else{
                let str = null;
                if(frequency==='monthly')
                    str = 'Currently your bill is generated on '+frequency+' basis. Would you like to change it to quarterly?'
                else
                    str = "Currently your bill is generated on "+frequency+" basis. Would you like to change it to monthly?";
                
                agent.add(new Suggestion('Yes'));
                agent.add(new Suggestion('No'));
                agent.add(str);
            }
        })
        .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'changebillfrequencyrequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
       // agent.clearContext('changebillfrequencyrequest-followup');
        let security_event = util.startSecurtiyCheck('Change Bill Frequency Request');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
               return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

changeBillFrequencyYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    
    return util.getCustBillingAcc(customer_id)
    .then((response) => {
        response = JSON.parse(response);
        let billingCycle = response.customerBillingCycleSpecification;
        let frequency = billingCycle.frequency;
        let billingDateShift = billingCycle.billingDateShift;
        let currDate = new Date();
        let lastBillingDate = new Date(response.bills[0].billDate.substring(0,response.bills[0].billDate.indexOf('T')));
        let nextBillDate = new Date(response.bills[0].nextBillDate.substring(0,response.bills[0].nextBillDate.indexOf('T')));
        let desiredBillDate = null;
        let str = null;

        //monthly to quarterly
        if(frequency==='monthly'){
            //change bill frequency
            if(((nextBillDate.getTime() - currDate.getTime())/(1000*60*60*24)) <= 7){
                desiredBillDate = new Date(nextBillDate);
                desiredBillDate.setMonth(desiredBillDate.getMonth()+3)
                str = "Your current bill is yet to be processed and scheduled for bill run on "+nextBillDate.toDateString()+". "
						+ "Once we dispatch this bill to you, the subsequent bill will be generated on quarterly basis from "
						+desiredBillDate.toDateString()+" onwards. Should I proceed with the change?";
            }
            //checking with next billing cycle
            else{
                desiredBillDate = new Date(lastBillingDate);
                desiredBillDate.setMonth(desiredBillDate.getMonth()+3)
                desiredBillDate.setDate(billingDateShift);
                str = "Your last bill was generated on "+lastBillingDate.toDateString()+" and has been sent to you. The subsequent bill will"
						+ " be generated on quarterly basis from "+desiredBillDate.toDateString()+" onwards. Should I proceed with the change?";
            }
        }
        //quarterly to monthly
        else{
            let difference = nextBillDate.getMonth()-currDate.getMonth()+(12*(nextBillDate.getFullYear()-currDate.getFullYear()));
            if(difference <= 1){
                console.log('Next Billing Date: '+nextBillDate);
                desiredBillDate = new Date(nextBillDate);
                desiredBillDate.setMonth(desiredBillDate.getMonth()+1);
                console.log('Desired Billing Date: '+desiredBillDate);
                console.log('New Next Billing Date: '+nextBillDate);
                str = "Your last quarter's is yet to be generated on "+nextBillDate.toDateString()+". "
						+ "Once we dispatch this bill to you, the subsequent bill will be generated on monthly basis from "
						+desiredBillDate.toDateString()+" onwards.";
            }
            else{
                let diff = nextBillDate.getMonth()-currDate.getMonth()+(12*(nextBillDate.getFullYear()-currDate.getFullYear()));
                desiredBillDate = new Date(lastBillingDate);
                desiredBillDate.setMonth(desiredBillDate.getMonth()+diff);
                desiredBillDate.setDate(billingDateShift);
                str = "Your last bill was sent on "+lastBillingDate.toDateString()+". Henceforth, it will be sent on monthly basis from "
						+desiredBillDate.toDateString()+" onwards. Should I proceed to change the bill frequency?";
            }
        }
        let billFrequencyContext = null;
        if(desiredBillDate!==null){
            billFrequencyContext = {
                'name' : 'changebillfrequencyrequest-yes-followup',
                'lifespan' : 1,
                'parameters' : {
                    'customer_id' : customer_id,
                    'desired_bill_date' : desiredBillDate.toISOString().slice(0, -5)
                }
            }
        }
        else{
            billFrequencyContext = {
                'name' : 'changebillfrequencyrequest-yes-followup',
                'lifespan' : 1,
                'parameters' : {
                    'customer_id' : customer_id
                }
            }
        } 
        agent.setContext(billFrequencyContext);
        agent.add(str);
        agent.add(new Suggestion('Yes'));
        agent.add(new Suggestion('No'));
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

changeBillFrequencyYesYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let nextBillDate = params.desired_bill_date;

    return util.getCustBillingAcc(customer_id)
    .then((response) => {
        response =JSON.parse(response);
        let billingCycle = response.customerBillingCycleSpecification;
        let frequency = billingCycle.frequency;
        let str = null;
        //monthly to quarterly
		if(frequency==='monthly'){
            return util.changeBillFreq(response._id,'quarterly',nextBillDate)
            .then((response) => agent.add('Your bill frequency has been successfully changed to quarterly.'));
        }
        //quarterly to monthly
        else{
            return util.changeBillFreq(response._id,'monthly',nextBillDate)
            .then((response) => agent.add('Your bill frequency has been successfully changed to monthly.'));
        }
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

module.exports.changeBillFrequency = changeBillFrequency;
module.exports.changeBillFrequencyYes = changeBillFrequencyYes;
module.exports.changeBillFrequencyYesYes = changeBillFrequencyYesYes;