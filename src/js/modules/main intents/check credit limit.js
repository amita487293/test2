const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

checkCreditLimit = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'Checkcreditlimit-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('Checkcreditlimit-followup');
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        return util.getCustBillingAcc(customer_id)
        .then((response) => {
            let creditLimit_v = JSON.parse(response).creditLimit;
            let creditLimit_val = creditLimit_v.value;
            let creditLimit_u = JSON.parse(response).creditLimit;
            let creditLimit_unit = creditLimit_u.unit;
            let value = params.amount;
            let unit = params.currency;
            let str = null;

            if(value!=null && !value.isEmpty())
				str="Sure, let me check this for you. Your current credit limit is "+creditLimit_val+" "+creditLimit_unit+". Would you like to change the limit to "+value+" "+unit+"?";
			else{
				str="Sure, let me check this for you. Your current credit limit is "+creditLimit_val+" "+creditLimit_unit+". Would you like to change the limit?";
                
                agent.add(str);
                agent.add(new Suggestion('Yes'));
                agent.add(new Suggestion('No'));
            }
        })
        .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'Checkcreditlimit-followup',
            lifespan : -1
        }
        agent.setContext(context);
        let security_event = util.startSecurtiyCheck('Check Credit Limit');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
               return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

checkCreditLimitYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let value = params.amount;
    let unit = params.currency;
    let srNum = util.generateRandomNumber();

    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        cust_bill_acc = custBillingAcc._id;
      return util.changeCustCreditLimit(cust_bill_acc,value)
       .then((vals) => {
        let str = "Sure, please take down your Service Request ID SR"+srNum+" and we will update you with the status within 24 hours.";
        agent.add(str);
       })

    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

module.exports.checkCreditLimit = checkCreditLimit;
module.exports.checkCreditLimitYes = checkCreditLimitYes;