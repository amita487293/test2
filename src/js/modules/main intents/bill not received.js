const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');
const datefun = require('date-and-time');


billNotReceivedIssue = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'BillNotReceivedIssue-followup',
            lifespan : -1
        }
        agent.setContext(context);
       // agent.clearContext('BillNotReceivedIssue-followup');
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        
        let followupEvent;

        return util.getCustBillingAcc(customer_id)
        .then((custBillingAcc) => {
            custBillingAcc = JSON.parse(custBillingAcc);
            let billmode = custBillingAcc.customerBillPresentationMedia.name;
            let billdate = custBillingAcc.customerBillingCycleSpecification.billingDateShift;
            if(custBillingAcc != null && billmode != null && billdate != null){
                let today = new Date();
                let date = util.getProperDate(billdate);
                let test = datefun.format(today,`YYYY-MM-${billdate}`);
                let billeddate = datefun.format(today,`YYYY-MM-DD`);
                let flag = test > billeddate;
                if(flag == true){
                    if((today.getMonth()+1)==1){
                        test = (today.getFullYear()-1)+"-"+(util.getProperDate((today.getMonth()+1)-1))+"-"+date;
                    }
                    else{
                        test = today.getFullYear()+"-"+(util.getProperDate((today.getMonth()+1)-1))+"-"+date;
                    }
                }
                let data = {
                    'parameters':{
                        'billeddate': test
                    }
                };
                let billeddate1 = datefun.parse(test, 'YYYY-MM-DD');  
                let test1 = datefun.parse(billeddate, 'YYYY-MM-DD');
                let dayselapsed = datefun.subtract(test1, billeddate1).toDays();
                if(billmode.includes('Paper')){
                    if(dayselapsed > 11){
                        data.name = 'Followup_Papergt11';
                    }
                    else{
                        data.parameters.checkdate = (today.getFullYear()+"-"+((today.getMonth()+1)-1)+"-"+(date+11));
                        data.name = 'Followup_Paperlt11';
                    }
                }
                else{
                    return util.getCustomer(customer_id)
                    .then((response) =>{
                        response = JSON.parse(response);
                        let arr1 = response.contactMedium;
                        let email = arr1[0].medium.emailAddress;
                    if(email != null){
                        data.parameters.email = email;
                        if(dayselapsed>2){
                            data.name = 'Followup_Ebillgt2';
                            //agent.setFollowupEvent('Followup_Ebillgt2');
                        }
                        else{
                            data.parameters.checkdate = (today.getFullYear()+"-"+((today.getMonth()+1)-1)+"-"+(date+2));
                            data.name = 'Followup_Ebillt2';
                        }
                        
                    }
                    else{
                       // agent.setFollowupEvent('live_agent');
                        followupEvent = util.startLiveAgent1('There is some issue we are facing with this request, let me transfer you to a live agent');
                        data = followupEvent;
                    }
                    agent.setFollowupEvent(data);
                })
                .catch((err) => {
                    console.log(err);
                    agent.add('This service is temporarily down. We are working to resolve it.');
                    return Promise.resolve();
                });
                }
            }

        })
        .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'BillNotReceivedIssue-followup',
            lifespan : -1
        }
        agent.setContext(context);
        //agent.clearContext('BillNotReceivedIssue-followup');
        let security_event = util.startSecurtiyCheck('Bill Not Received Issue');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
               return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

ebillElapsedGT2IssueYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    return util.createTroubleTicket(params)
   .then((response)=>{
        agent.add("A Trouble Ticket TT"+response._id+" has been raised in the system to send the Ebill to you again.")
   })
   .catch((err) => {
        console.log(err);
        agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });  
}

ebillElapsedGT2IssueNoYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let email_id = params.new_email;
    return util.changeCustEmailId(customer_id,email_id)
    .then(()=>{
        let response = util.createTroubleTicket(params);
        agent.add("Yes, your email id is now verified and a Trouble Ticket TT "+response._id+" has been raised in the system to send the Ebill to you again.");
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it. 181');
        return Promise.resolve();
    });
}

ebillElapsedGT2IssueNoNo = (agent) =>{
    let followupEvent = util.startLiveAgent1("We will not be able to process your request without a correct email. Let me transfer you to a Live Agent for assistance.");
    agent.setFollowupEvent(followupEvent);
}

paperBillElapsedGT11Issue = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let response = util.createTroubleTicket(params);
    let billed_date = params.billed_date;
    agent.add("Apologies for the inconvenience. As I am seeing, you have subscribed to Paper bill. Your bill was generated on " + billed_date + " and should have reached by now. I will arrange for the bill to be resent to your registered address. A Trouble Ticket TT" +response._id+ " has been raised in the system to dispatch the bill to you again.");
}

module.exports.billNotReceivedIssue = billNotReceivedIssue;
module.exports.ebillElapsedGT2IssueYes = ebillElapsedGT2IssueYes;
module.exports.ebillElapsedGT2IssueNoYes = ebillElapsedGT2IssueNoYes;
module.exports.ebillElapsedGT2IssueNoNo = ebillElapsedGT2IssueNoNo;
module.exports.paperBillElapsedGT11Issue = paperBillElapsedGT11Issue;