const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

changeBillingDate = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'changebillingdaterequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
       // agent.clearContext('changebillingdaterequest-followup');
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        return util.getCustBillingAcc(customer._id)
        .then((response) => {
            response = JSON.parse(response);
            let paymentMean = response.paymentMean.name;
            
            if(paymentMean === 'direct debit'){
                let billingCycle = response.customerBillingCycleSpecification;
                let billDate = billingCycle.billingDateShift;
                let frequency = billingCycle.frequency;
                agent.add("Your current "+frequency+" billing date is "+util.ordinal(billDate)+". What date would you like to change this to?");
            }
            else{
                let context = {
                    name : 'changebillingdaterequest-followup',
                    lifespan : -1
                }
                agent.setContext(context);
              //  agent.clearContext('changebillingdaterequest-followup');
                agent.add('I am unable to change your billing date as your payment method is not direct debit, you need to change your payment setup to Direct Debit first');
                agent.add(new Suggestion('Direct Debit Setup'));
            }
        })
        .catch((err) => {
            console.log(err);
            return Promise.resolve();
        });
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'changebillingdaterequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('changebillingdaterequest-followup');
        let security_event = util.startSecurtiyCheck('Change Billing Date Request');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
               return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            console.log(err);
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

changeBillingDateValid = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let billDate = params.ordinal;

    return util.getCustBillingAcc(customer_id)
        .then((response) => {
            response = JSON.parse(response);
            if(billDate==0)
                billDate = params.number;
            if(billDate!==response.customerBillingCycleSpecification.billingDateShift){
                let str = null;
                //valid date
				if(billDate >= 2 && billDate <= 28){
                    let currDate = new Date();
                    let proposedDate = new Date(currDate.getFullYear()+'-'+(currDate.getMonth()+1)+'-'+util.getProperDate(billDate));
                    let lastBillingDate = new Date(response.bills[0].billDate.substring(0,response.bills[0].billDate.indexOf('T')));
                    let nextBillDate = new Date(response.bills[0].billDate.substring(0,response.bills[0].nextBillDate.indexOf('T')));
                    
                    if(proposedDate<currDate)
                        proposedDate.setMonth(proposedDate.getMonth() + 1);
                    
                    if(((proposedDate.getTime() - currDate.getTime())/(1000*60*60*24)) >= 15){
                        //3rd case
                        if(((nextBillDate.getTime() - currDate.getTime())/(1000*60*60*24)) > 3){
                            lastBillingDate.setDate(lastBillingDate.getDate()+1);
                            str = "Your next bill will be generated on "+proposedDate.toDateString()+". You will be charged on pro-rata basis from "+lastBillingDate.toDateString()+" to "+proposedDate.toDateString()+". Should I change the billing date to "+util.ordinal(billDate)+"?";
                            nextBillDate = new Date(proposedDate);
                        }
                        else{
                            if(proposedDate < nextBillDate)
                                proposedDate.setMonth(proposedDate.getMonth() + 1);
                            
                            lastBillingDate.setDate(lastBillingDate.getDate()+1);
                            str = "Your current bill will be generated on "+nextBillDate.toDateString()+". You will be charged on pro-rata basis from "+lastBillingDate.toDateString()+" to "+proposedDate.toDateString()+". Should I change the billing date to "+util.ordinal(billDate)+"?";
                            nextBillDate = new Date(proposedDate);
                        }
                    }
                    else{
                        lastBillingDate.setDate(lastBillingDate.getDate()+1);
                        proposedDate.setMonth(proposedDate.getMonth()+1);
                        str = "Your next bill will be generated on "+proposedDate.toDateString()+". You will be charged on pro-rata basis from "+lastBillingDate.toDateString()+" to "+proposedDate.toDateString()+". Should I change the billing date to "+util.ordinal(billDate)+"?";
						nextBillDate = new Date(proposedDate);
                    }
                    
					let changeBillContext = {
                        'name':'changebillingdaterequest-valid-followup',
                        'lifespan' : 1,
                        'parameters' : {
                            'customer_id' : customer_id,
                            'number' : util.getProperDate(billDate),
                            'nextbilldate' : nextBillDate.toISOString().slice(0, -5)
                        }
                    }
                    agent.setContext(changeBillContext);
                    agent.add(str);
                    agent.add(new Suggestion('Yes'));
                    agent.add(new Suggestion('No'));
				}
				//invalid date
				else{
                    let context = {
                        name : 'changebillingdaterequest-valid-followup',
                        lifespan : -1
                    }
                    agent.setContext(context);
                  //  agent.clearContext('changebillingdaterequest-valid-followup')
					agent.add("Sorry, it can’t be changed to "+util.ordinal(billDate)+". Please try again with any other date between 2nd and 28th of the month");
				}
            }
            else{
                let context = {
                    name : 'changebillingdaterequest-valid-followup',
                    lifespan : -1
                }
                agent.setContext(context);
              //  agent.clearContext('changebillingdaterequest-valid-followup');
                agent.add('Your billing date is already '+util.ordinal(billDate)+'.');
            }
        })
        .catch((err) => {
            console.log(err);
            agent.add('This service is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
}

changeBillingDateValidYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let billDate = params.number;
    let nextBillDate = params.nextbilldate;

    return util.getCustBillingAcc(customer_id)
    .then((response) => {
        response = JSON.parse(response);
        return util.changeBillDate(response._id, billDate, nextBillDate)
        .then((response) => agent.add("Ok, your billing date has been changed successfully to "+util.ordinal(billDate)));
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

module.exports.changeBillingDate = changeBillingDate;
module.exports.changeBillingDateValid = changeBillingDateValid;
module.exports.changeBillingDateValidYes = changeBillingDateValidYes;