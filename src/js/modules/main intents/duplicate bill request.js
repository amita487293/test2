const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');
const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

duplicateBillRequest = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'duplicatebillrequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);
        if(params.period.length>0)
            agent.setFollowupEvent('duplicatebillrequest-period');
        else{

            agent.add("Sure! Which 'month year' do you need the bill for?");
            let today = new Date();
            let d;
            let month;
            let year;
            for(var i = 0; i < 11; i++) {
                d = new Date(today.getFullYear(), today.getMonth() - (i+1), 1);
                month = monthNames[d.getMonth()];
                year = d.getFullYear();
                agent.add(new Suggestion(month+' '+year));
            }
        }
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'duplicatebillrequest-followup',
            lifespan : -1
        }
        agent.setContext(context);
        let security_event = util.startSecurtiyCheck('Duplicate Bill Request');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
                return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='GOOGLE_TELEPHONY'){
       console.log("In duplicate telephony Testing App11");
       agent.add('In Duplicate bill 111');

        securityCheck();
    }
    else 
        securityCheckSkip();

}

duplicateBillRequestPeriod = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let period = params.period;
    let customer_id = params.customer_id;

    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        let billSubscriptionType = custBillingAcc.customerBillPresentationMedia.name;
        let startPeriod = period.startDate.substring(0,period.startDate.indexOf('T'));
        let startDate = new Date(startPeriod);
        let currDate = new Date();
        let diff = currDate.getMonth()-startDate.getMonth()+(12*(currDate.getFullYear()-startDate.getFullYear()));

        if(diff > 0 && diff <= 12){
            let str = null;
            if(billSubscriptionType.toLowerCase().includes('paper')){
                agent.add("You have subscribed to paper bill, say 'paper bill' to confirm.");
                agent.add(new Suggestion('paper bill'));  
            }
            else{
                agent.add('Do you want paper bill or ebill or both?');
                agent.add(new Suggestion('paper bill'));
                agent.add(new Suggestion('ebill'));
                agent.add(new Suggestion('both'));       
            }
        }
        else if(diff===0){
            let live_agent = {
                'name':'live_agent',
                'parameters':{
                    'prompt':'Your current month bill has not been processed yet. You can request a duplicate bill upto 12 months from last month. Let me connect you with a live agent.'
                }
            };
            agent.setFollowupEvent(live_agent);
        }
        else{
            let live_agent = {
                'name':'live_agent',
                'parameters':{
                    'prompt':'You have entered invalid date. You can request a duplicate bill upto 12 months from last month. Let me connect you with a live agent.'
                }
            };
            agent.setFollowupEvent(live_agent);
        }
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

duplicateBillRequestPeriodChoice = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let choice = params.choice;
    let customer_id = params.customer_id;
    let period = params.period;

    if(choice === 'paper' || choice === 'both'){
        let paperEvent = {
            'name':'paper_bill',
            'parameters':{
                'customer_id':customer_id,
                'period':period,
                'choice':choice
            }
        }
        agent.setFollowupEvent(paperEvent);
    }
    else{
        let ebillEvent = {
            'name':'ebill',
            'parameters':{
                'customer_id':customer_id,
                'period':period,
                'choice':choice
            }
        }
        agent.setFollowupEvent(ebillEvent);
    }
}

paperBill = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    return util.getCustBillingAcc(customer_id).then((response) => {
        response = JSON.parse(response);
        let currencyCode = response.currency.currencyCode;
        agent.add('Ok, Paper bill will include service + shipping charge of 0.7 '+currencyCode+'. Are you ok with this charge?');
        agent.add(new Suggestion('yes'));
        agent.add(new Suggestion('no'));
    });
}

paperBillYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let choice = params.choice;
    console.log("Choice is :"+choice);
    return util.getCustomer(customer_id).then((customer) => {
        customer = JSON.parse(customer);
        let contactMedium = customer.contactMedium[0].medium;
        let street1 = contactMedium.street1;
        let city = contactMedium.city;
        let country = contactMedium.country;
        let email = contactMedium.emailAddress;
        let  str = null;
        if(choice === 'both'){
            if(email.length<2)
                str = "I have generated a request with ID BB435 and you shall receive your bill within next 11 days. The bill "
                + "will be sent to the address- "+street1+", "+city+", "+country+" and your email has not been linked to your profile. Kindly link it to get the ebill";
            else
                str = "I have generated a request with ID BB435 and you shall receive your bill within next 11 days. The bill "
                + "will be sent to the address- "+street1+", "+city+", "+country+" and your ebill will be sent to "+email+" within next 2 days.";
        }
        else
            str = "I have generated a request with ID BB435 and you shall receive your bill within next 11 days. The bill "
            + "will be sent to the address- "+street1+", "+city+", "+country;
    
        agent.add(str);
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

paperBillNo = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    let choice = params.choice;
    if(choice === 'yes'){
        let ebillEvent = {
            'name':'ebill',
            'parameters':{
                'customer_id':customer_id,
                'period':period
            }
        }
        agent.setFollowupEvent(ebillEvent);
    }
    else
        agent.add('Apologies, I can’t dispatch the duplicate paper bill without this charge');
}

eBill = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    return util.getCustomer(customer_id).then((customer) => {
        customer = JSON.parse(customer);
        let contactMedium = customer.contactMedium[0].medium;
        let email = contactMedium.emailAddress;
        return util.getCustBillingAcc(customer_id).then((custBillingAcc) => {
            custBillingAcc = JSON.parse(custBillingAcc);
            let billSubscriptionType = custBillingAcc.customerBillPresentationMedia.name;
            if(billSubscriptionType.toLowerCase().includes('paper')){
                agent.add("You have subscription to receive only paper bill. Kindly 'switch to ebill' to receive ebills in future");
                agent.add(new Suggestion('Switch to Ebill'));
            }
            else
                agent.add("A request has been generated with ID BB445 and you shall receive your bill within next 2 days. The bill "
                + "will be sent to "+email);
        });
    });
}

module.exports.duplicateBillRequest = duplicateBillRequest;
module.exports.duplicateBillRequestPeriod = duplicateBillRequestPeriod;
module.exports.duplicateBillRequestPeriodChoice = duplicateBillRequestPeriodChoice;
module.exports.paperBill = paperBill;
module.exports.paperBillYes = paperBillYes;
module.exports.paperBillNo = paperBillNo;
module.exports.eBill = eBill;