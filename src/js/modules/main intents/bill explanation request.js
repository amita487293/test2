const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

billExplanationResolved = (agent) => {
    proceed = () => {
            agent.setFollowupEvent('end_event');
        }
    proceed();
}

billExplanationUnresolved = (agent) => {
    let live_agent = {
        'name':'live_agent',
        'parameters':{
            'prompt':'Let me get you one of our experts straight away.'
        }
    };
    agent.setFollowupEvent(live_agent);
}

module.exports.billExplanationResolved = billExplanationResolved;
module.exports.billExplanationUnresolved = billExplanationUnresolved;