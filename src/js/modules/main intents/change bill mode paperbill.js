const Util = require('../utils/util');
const util = new Util();
const {Suggestion} = require('dialogflow-fulfillment');

changeBillModePaperBill = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id.toString();
    let payload = request.originalDetectIntentRequest.payload?request.originalDetectIntentRequest.payload.data:null;

    //unidentified customer from selfcare
    securityCheckSkip = () => {
        let context = {
            name : 'ChangeBillMode-PaperBill-followup',
            lifespan : -1
        }
        agent.setContext(context);
       // agent.clearContext('ChangeBillMode-PaperBill-followup');
        agent.add('This request requires you to login into selfcare.');
    }
    //identified customer
    proceed = (customer) => {
        let welcomeContext = {
            'name' : 'welcome',
            'lifespan' : 1,
            'parameters' : {
                'customer_id' : customer._id
            }
        }
        agent.setContext(welcomeContext);

    //new code start
    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {

        custBillingAcc = JSON.parse(custBillingAcc);
        let billmode = custBillingAcc.customerBillPresentationMedia.name;
        console.log("bill mode"+billmode);
        let billModeChange = params.bill_mode;
        console.log(billModeChange);
        let arr = customer.contactMedium;
        console.log(arr);
        let city = arr[0].medium.city;
        let street1 = arr[0].medium.street1;
        let street2 = arr[0].medium.street2;
        
        if(billmode != null && billModeChange == "Paperbill" && billmode.includes("Ebill")){
            if((city!=null) && (street1!=null || street2!=null)){
                agent.add(`Let me check if your bill mode can be changed from Ebill  to Paperbill from next billing cycle. You will receive the Paperbill from next billing cycle at "${street1}, ${street2}, ${city}". Is the address correct?`);
                agent.add(new Suggestion('yes'));
                agent.add(new Suggestion('no'));
            }
        }
        else if(billmode.includes("Paper")){
            agent.add("Your bill mode is already Paperbill.");
        }
        else{
            //set output context to go to yesyes
            let updatedContext = {
                'name' : 'ChangeBillMode-PaperBill-yes-yes-followup',
                'lifespan' : 3
            }
            agent.setContext(updatedContext);
            agent.add("You have subscribed to both email and paper bills.Do you want to continue ebill?");
            agent.add(new Suggestion('yes'));
            agent.add(new Suggestion('no'));

        }
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
    //end code
    }
    //unidentified customer and not from selfcare
    securityCheck = () => {
        let context = {
            name : 'ChangeBillMode-PaperBill-followup',
            lifespan : -1
        }
        agent.setContext(context);
      //  agent.clearContext('ChangeBillMode-PaperBill-followup');
        let security_event = util.startSecurtiyCheck('Change Bill Mode - PaperBill');
        agent.setFollowupEvent(security_event);
    }

    if(customer_id.length>0) 
        return util.getCustomer(customer_id)
        .then((response) =>{
            if(response)
                return proceed(JSON.parse(response));
            else
                agent.add('Invalid customer ID found. Please try again from any valid channel');
        })
        .catch((err) => {
            agent.add('Our selfcare portal is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    //Whatsapp
    else if(payload!=null && payload.hasOwnProperty("id_whatsapp") && payload.id_whatsapp.length>0){
        let id_whatsapp = payload.id_whatsapp;
        return util.getCustomerWhatsapp(id_whatsapp)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err)=>{
            agent.add('Service unavailable at the moment.');
            return Promise.resolve();
        });
    }
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='google') 
        return util.getCustomerGoogle(request.originalDetectIntentRequest.payload.user.userId)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })
        .catch((err) => {
            agent.add('Our google channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='facebook')
        return util.getCustomerFB(request.originalDetectIntentRequest.payload.data.sender.id)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our facebook channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else if(request.originalDetectIntentRequest.hasOwnProperty('source') && request.originalDetectIntentRequest.source==='twitter')
        return util.getCustomerTwitter(request.originalDetectIntentRequest.payload.data.direct_message.sender_id_str)
        .then((response) => {
            if(response)
                return proceed(JSON.parse(response));
            else
                securityCheck();
        })        
        .catch((err) => {
            agent.add('Our twitter channel is temporarily down. We are working to resolve it.');
            return Promise.resolve();
        });
    else 
        securityCheckSkip();
}

changeBillModePaperBillNo = (agent) =>{
    let updatedContext = {
        'name' : 'ChangeBillMode-PaperBill-followup',
        'lifespan' : -1
    }
    agent.add("If you want the Paperbill to be sent to different address, you will need to change/update address in the system before placing the request for bill mode change");
    agent.add(new Suggestion('Change Billing Address'));
    agent.setContext(updatedContext);
}

changeBillModePaperBillYes = (agent) =>{
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let billModeChange = params.bill_mode;
    agent.add(`You will be charged ${billModeChange} surcharge of 0.7 EUR per month. Are you ok to pay the surcharge?`);
    agent.add(new Suggestion('yes'));
    agent.add(new Suggestion('no'));
}

changeBillModePaperBillYesNo = (agent) =>{
    agent.add("Ok, your bill mode will be retained as Ebill.");
}

changeBillModePaperBillYesYes = (agent) =>{
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let billModeChange = params.bill_mode;
    agent.add(`Do you want to continue to receive Ebill in addition to ${billModeChange} ?`);
    agent.add(new Suggestion('yes'));
    agent.add(new Suggestion('no'));
}

changeBillModePaperBillYesYesYes = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        let billmode = custBillingAcc.customerBillPresentationMedia.name;
        return util.changeBillMode(custBillingAcc._id,"Both")
            .then(() =>{
                agent.add("Thank you, you will receive both Ebill and Paperbill from next billing cycle")
            })
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

changeBillModePaperBillYesYesNo = (agent) => {
    let request = agent.request_.body;
    let params = request.queryResult.parameters;
    let customer_id = params.customer_id;
    return util.getCustBillingAcc(customer_id)
    .then((custBillingAcc) => {
        custBillingAcc = JSON.parse(custBillingAcc);
        let billmode = custBillingAcc.customerBillPresentationMedia.name;
        return util.changeBillMode(custBillingAcc._id,"Paper invoice")
            .then(() =>{
                agent.add("Thank you, you will get only Paperbill from next billing cycle")
            })
    })
    .catch((err) => {
        console.log(err);
        agent.add('This service is temporarily down. We are working to resolve it.');
        return Promise.resolve();
    });
}

module.exports.changeBillModePaperBill = changeBillModePaperBill;
module.exports.changeBillModePaperBillNo = changeBillModePaperBillNo;
module.exports.changeBillModePaperBillYes = changeBillModePaperBillYes;
module.exports.changeBillModePaperBillYesNo = changeBillModePaperBillYesNo;
module.exports.changeBillModePaperBillYesYes = changeBillModePaperBillYesYes;
module.exports.changeBillModePaperBillYesYesYes = changeBillModePaperBillYesYesYes;
module.exports.changeBillModePaperBillYesYesNo = changeBillModePaperBillYesYesNo;