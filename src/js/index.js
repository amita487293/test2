const express = require('express');
const app = express();


const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
const welcome = require('./modules/support intents/welcome');
const flback = require('./modules/support intents/fallback');
const integration = require('./modules/support intents/integration');
const secCheck = require('./modules/support intents/security check');
const dupBillReq = require('./modules/main intents/duplicate bill request');
const billFreq = require('./modules/main intents/change bill frequency');
const billDate = require('./modules/main intents/change billing date');
const chngEbill = require('./modules/main intents/change bill mode ebill');
const chngPaperbill = require('./modules/main intents/change bill mode paperbill');
const chngPaymtCredit = require('./modules/main intents/change payment method credit');
const chngPaymtMethod = require('./modules/main intents/change payment method');
const chngPaymtDebitAcc = require('./modules/support intents/change payment method debit acc');
const billexpreq = require('./modules/main intents/bill explanation request');
const validationChe = require('./modules/support intents/validation');
const checkCreditlim = require('./modules/main intents/check credit limit');
const customerBillAddUpReq = require('./modules/main intents/customer billing address update req');
const endingNote = require('./modules/support intents/endingnote');
const payBill = require('./modules/main intents/pay bill');
const otpGen = require('./modules/support intents/otp generate');
const billNotreceived = require('./modules/main intents/bill not received');

app.use(express.json());

app.post('/webhook',(request,response)=>{
    let agent = new WebhookClient({request,response});
    
    //Intent mapping
    let intentMap = new Map();
    intentMap.set('Default Welcome Intent', welcome);
    intentMap.set('Default Fallback Intent - yes', flback.fallbackYes);
    intentMap.set('Google Integration', integration.googleWelcome);
    intentMap.set('Google Integration - yes', integration.googleIntegration);
    intentMap.set('FB Integration', integration.facebookWelcome);
    intentMap.set('FB Integration - yes', integration.facebookIntegration);
    intentMap.set('Twitter Integration', integration.twitterWelcome);
    intentMap.set('Twitter Integration - yes', integration.twitterIntegration);
    intentMap.set('Whatsapp Integration', integration.whatsappWelcome);
    intentMap.set('Whatsapp Integration - yes', integration.whatsappIntegration);
    intentMap.set('Security Check', secCheck.securityCheck);
    intentMap.set('Security Check 1', secCheck.securityCheck1);
    intentMap.set('Security Check 2', secCheck.securityCheck2);
    
    intentMap.set('Duplicate Bill Request', dupBillReq.duplicateBillRequest);
    intentMap.set('Duplicate Bill Request - period', dupBillReq.duplicateBillRequestPeriod);
    intentMap.set('Duplicate Bill Request - period - choice', dupBillReq.duplicateBillRequestPeriodChoice);
    intentMap.set('Paper Bill',dupBillReq.paperBill);
    intentMap.set('Paper Bill - yes',dupBillReq.paperBillYes);
    intentMap.set('Paper Bill - no',dupBillReq.paperBillNo);
    intentMap.set('EBill',dupBillReq.eBill);

    intentMap.set('Change Bill Frequency Request',billFreq.changeBillFrequency);
    intentMap.set('Change Bill Frequency Request - yes',billFreq.changeBillFrequencyYes);
    intentMap.set('Change Bill Frequency Request - yes - yes',billFreq.changeBillFrequencyYesYes);

    intentMap.set('Change Billing Date Request',billDate.changeBillingDate);
    intentMap.set('Change Billing Date Request - valid',billDate.changeBillingDateValid);
    intentMap.set('Change Billing Date Request - valid - yes',billDate.changeBillingDateValidYes);

    intentMap.set('Change Bill Mode Ebill',chngEbill.changeBillModeEbill);
    intentMap.set('Change Bill Mode Ebill - yes',chngEbill.changeBillModeEbillYes);
    intentMap.set('Change Bill Mode Ebill - yes - no',chngEbill.changeBillModeEbillYesNo);
    intentMap.set('Change Bill Mode Ebill - yes - yes',chngEbill.changeBillModeEbillYesYes);
    intentMap.set('Change Bill Mode Ebill - yes - email',chngEbill.changeBillModeEbillYesEmail);

    intentMap.set('Change Bill Mode - PaperBill',chngPaperbill.changeBillModePaperBill);
    intentMap.set('Change Bill Mode - PaperBill - no',chngPaperbill.changeBillModePaperBillNo);
    intentMap.set('Change Bill Mode - PaperBill - yes',chngPaperbill.changeBillModePaperBillYes);
    intentMap.set('Change Bill Mode - PaperBill - yes - no',chngPaperbill.changeBillModePaperBillYesNo);
    intentMap.set('Change Bill Mode - PaperBill - yes - yes',chngPaperbill.changeBillModePaperBillYesYes);
    intentMap.set('Change Bill Mode - PaperBill - yes - yes - yes',chngPaperbill.changeBillModePaperBillYesYesYes);
    intentMap.set('Change Bill Mode - PaperBill - yes - yes - no',chngPaperbill.changeBillModePaperBillYesYesNo);

    intentMap.set('Change Payement Method - Credit',chngPaymtCredit.changePaymentMethodCredit);
    intentMap.set('Change Payement Method - Credit - Paytype',chngPaymtCredit.changePaymentMethodCreditPaytype);
    intentMap.set('Change Payement Method - Credit - Paytype - no',chngPaymtCredit.changePaymentMethodCreditPaytypeNo);
    intentMap.set('Change Payement Method - Credit - Paytype -validation',chngPaymtCredit.changePaymentMethodCreditPaytypeValidation);
    intentMap.set('Change Payement Method - Credit - Paytype -validation - Fail',chngPaymtCredit.changePaymentMethodCreditPaytypeValidationFail);
    intentMap.set('Change Payement Method - Credit - Paytype -validation - Pass',chngPaymtCredit.changePaymentMethodCreditPaytypeValidationPass);
    intentMap.set('Change Payement Method - Credit - Paytype -validation - Pass - OTP',chngPaymtCredit.changePaymentMethodCreditPaytypeValidationPassOTP);

    intentMap.set('Change Payment Method',chngPaymtMethod.changePaymentMethod);
    intentMap.set('Change Payment Method - yes',chngPaymtMethod.changePaymentMethodYes);
    intentMap.set('Change Payment Method - yes - yes',chngPaymtMethod.changePaymentMethodYesYes);
    intentMap.set('Change Payment Method - yes - yes - yes',chngPaymtMethod.changePaymentMethodYesYesYes);

    intentMap.set('Change Payment Method - Debit - Acc',chngPaymtDebitAcc.changePaymentMethodDebitAcc);
    intentMap.set('Change Payment Method - Debit - Acc - Otp',chngPaymtDebitAcc.changePaymentMethodDebitAccOtp);
    intentMap.set('Joint Acc',chngPaymtDebitAcc.jointAcc);
    intentMap.set('Joint Acc - no',chngPaymtDebitAcc.jointAccNo);

    intentMap.set('Bill Explanation Request - resolved',billexpreq.billExplanationResolved);
    intentMap.set('Bill Explanation Request - unresolved',billexpreq.billExplanationUnresolved);

    intentMap.set('Validation Check',validationChe.validationCheck);
    intentMap.set('Validation Check - yes',validationChe.validationCheckYes);

    intentMap.set('Check Credit Limit',checkCreditlim.checkCreditLimit);
    intentMap.set('Check Credit Limit - yes',checkCreditlim.checkCreditLimitYes);

    intentMap.set('Customer Billing Address Update Request',customerBillAddUpReq.custBillAddrUpReq);
    intentMap.set('Customer Billing Address Update Request - yes',customerBillAddUpReq.CustBillAddUpReqYes);
    intentMap.set('Customer Billing Address Update Request - Locality',customerBillAddUpReq.CustBillAddUpReqLocality);
    intentMap.set('Locality - fallback',customerBillAddUpReq.CustBillAddUpReqLocalityFallback);

    intentMap.set('Ending Note',endingNote.endingNote);
    intentMap.set('Ending Note - yes',endingNote.endingNoteYes);

    intentMap.set('Pay Bill',payBill.payBill);   
    
    intentMap.set('OTP Generate',otpGen.otpGenerate);   

    intentMap.set('Bill Not Received Issue', billNotreceived.billNotReceivedIssue);
    intentMap.set('EBill Elapsed GT 2 Issue - yes', billNotreceived.ebillElapsedGT2IssueYes);
    intentMap.set('EBill Elapsed GT 2 Issue - no - yes', billNotreceived.ebillElapsedGT2IssueNoYes);
    intentMap.set('EBill Elapsed GT 2 Issue - no - no', billNotreceived.ebillElapsedGT2IssueNoNo);
    intentMap.set('Paper Bill Elapsed GT 11 Issue', billNotreceived.paperBillElapsedGT11Issue);
    
    agent.handleRequest(intentMap);
});

//PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening to port ${port}...`));